const modelDesc = {
    name: "Titanic Survival Model",
    algorithm: "Random Forest",
    description: "Prediction model trained on the Titanic passenger dataset.\nUsed scikit-learn's Random Forest algorithm.",
    accuracy: 0.776,
    precision: 0.78,
    recall: 0.78
};

export default modelDesc;