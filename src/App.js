import React, {Component} from 'react';
import './App.css';
import NavBar from './components/NavBar';
import Background from "./components/Background";
import InputData from "./components/InputData";
import ModelDesc from "./components/ModelDesc";
import About from "./components/About";
import './style.css';

class App extends Component {
    constructor() {
        super();
        this.state = {
            didClick:false
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        console.log(this.state.didClick);
        this.setState({
            didClick:true
        });
    }

    render() {
        return (
            <div>
                <div>
                    <NavBar /><br />
                    <div id="landing">
                        { this.state.didClick === false ?
                            <div id="landing-div">
                                <h1>
                                    Would you survive on the Titanic?
                                </h1>
                                <br />
                                <button
                                    id="fill-out-button"
                                    onClick={this.handleClick}
                                >Submit a ticket form</button>
                            </div> :
                                <InputData />
                        }
                    </div>
                    <Background />
                </div>
                <div id="model-outer">
                    <ModelDesc />
                </div>
                <div id="about-outer">
                    <About />
                </div>
            </div>
        );
    }
}

export default App;
