import React from 'react';
import {Link} from 'react-scroll';

function NavBar() {
    return (
      <div>
          <ul id="nav">
              <a href="/">
                <h2 id="home-button">T</h2>
              </a>
              <div id="nav-btn">
                  <Link
                      className="nav-link"
                      activeClass="active"
                      to="landing"
                      spy={true}
                      smooth={true}
                      offset={0}
                      duration={500}
                  >Home</Link>
                  <Link
                      className="nav-link"
                      activeClass="active"
                      to="model-outer"
                      spy={true}
                      smooth={true}
                      offset={0}
                      duration={500}
                  >Model</Link>
                  <Link
                      className="nav-link"
                      activeClass="active"
                      to="about-outer"
                      spy={true}
                      smooth={true}
                      offset={0}
                      duration={500}
                  >About</Link>
              </div>
          </ul>
      </div>
    );
}

export default NavBar;