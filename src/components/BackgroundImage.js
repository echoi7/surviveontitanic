import React from 'react';

function BackgroundImage(props) {
    return (
        <div>
            <div
                className="bg"
                id="bg-overlay"
            />
            <img
                src={props.url}
                className="bg"
                alt="Image of a ship"
                id="bg-img"
            />
        </div>
    )
}

export default BackgroundImage;