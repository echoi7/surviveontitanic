import React from 'react';
import modelDesc from '../modelDesc';

function ModelDesc() {
    return (
        <div className="content-div">
            <div className="content" id="model-desc">
                <h3>{modelDesc.name}</h3>
                <p>{modelDesc.description}</p>
                <p>
                    Accuracy: {modelDesc.accuracy} Precision: {modelDesc.precision} Recall: {modelDesc.recall}
                </p>
                <p>
                    Dataset: <a href="https://www.kaggle.com/c/titanic/">Titanic: Machine Learning From Disaster</a>
                </p>
            </div>
        </div>
    );
}

export default ModelDesc;