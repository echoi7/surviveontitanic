import React from 'react';

function ImageCredit(props) {
    return (
        <div className="footer">
            <p id="photo-location">
                {(props.city !== null) ? <text>{props.city}</text>: null}
                {(props.city !== null && props.country !== null) ? ", " : null}
                {(props.country !== null) ? <text>{props.country}</text> : null}

            </p>
            <p id="photo-credit"><a href={props.link}>By {props.photographer}</a></p>
        </div>
    );
}

export default ImageCredit;