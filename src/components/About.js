import React from 'react';

function About() {
    return (
        <div className="content-div">
            <h3>About this project</h3>
            {/*<p>This project was created by <a href="">Eugene Choi</a> to educate the public about how machine learning models work.*/}
            {/*Machine learning is an area of AI that is widely implemented in many of the applications we use.*/}
            {/*In the midst of blooming AI spring and the chaos it may bring due to lack of understanding and policies, it is hoped that people will eventually understand that AI itself is not to be feared.*/}
            {/*It is the lack of knowledge and lack of guidelines about how to use AI that may hurt us.*/}
            {/*AI is a wonderful technology that can be entertaining but also have tremendous amount of power that especially depends on the amount of data available and the knowledge of the wielder.*/}
            <p>
                Version 1.0 <br />
                Built by <a href="https://eugene-choi.me">Eugene Choi</a> with React, Django, Heroku, and a ton of coffee ☕️. <br />
            </p>
        </div>
    );
}

export default About;