import React from 'react';
import {
    EmailShareButton,
    FacebookShareButton,
    PinterestShareButton,
    RedditShareButton,
    TwitterShareButton,
    TelegramShareButton,
    WhatsappShareButton,
    TwitterIcon,
    FacebookIcon,
    PinterestIcon,
    RedditIcon,
    TelegramIcon,
    WhatsappIcon,
    EmailIcon} from 'react-share';


function ShareResults(props) {
    const description = {
        "short": props.survive === true ? "I survived" : "I did not survive" + " on the Titanic!",
        "long": "I found out that I " + props.survive === true ? "survived" : "did not survive" + " on the Titanic using Would You Survive On The Titanic!",
        "link": String(window.location),
        "tag": "#WouldYouSurviveOnTheTitanic"
    };
    const size = 60;
    const round = false;
    return(
        <div>
            Share your results! <br />
            <div id="share">
                <EmailShareButton
                    subject={description["short"]}
                    body={description["long"]}
                    url={description["link"]}
                ><EmailIcon size={size} /> </EmailShareButton>
                <FacebookShareButton
                    quote={description["long"]}
                    hashtag={description["tag"]}
                    url={description["link"]}
                ><FacebookIcon size={size} /> </FacebookShareButton>
                <PinterestShareButton
                    title={description["short"]}
                    summary={description["long"]}
                    source={description["link"]}
                    url={description["link"]}
                ><PinterestIcon size={size} /> </PinterestShareButton>
                <RedditShareButton
                    title={description["short"]}
                    url={description["link"]}
                > <RedditIcon size={size} /> </RedditShareButton>
                <TwitterShareButton
                    title={description["short"]}
                    url={description["link"]}
                > <TwitterIcon size={size} /> </TwitterShareButton>
                <WhatsappShareButton
                    title={description["short"]}
                    url={description["link"]}
                > <WhatsappIcon size={size} /> </WhatsappShareButton>
                <TelegramShareButton
                    title={description["short"]}
                    url={description["link"]}
                > <TelegramIcon size={size} /> </TelegramShareButton>
            </div>
            {/*<div>*/}
                {/*Copy Link:*/}
                {/*<input*/}
                    {/*type="text"*/}
                    {/*value={description["link"]}*/}
                    {/*id="copy-link"*/}
                    {/*onClick={this.handleClick}*/}
                {/*/>*/}
            {/*</div>*/}
        </div>
    );
}

export default ShareResults;