import React, {Component} from 'react';
import ShareResults from './ShareResults';

class InputData extends Component {
    constructor() {
        super();
        this.state = {
            ticket_class:1,
            sex:"female",
            age:0,
            siblings:0,
            spouse:0,
            parents:0,
            children:0,
            fare:50,
            embarked:"C",
            survive:false,
            hasSubmitted: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }


    handleChange(event) {
        const {name, value} = event.target;
        let curr_fare = this.state.ticket_class === 1 ? 50 : (this.state.ticket_class === 2 ? 20 : 10);
        if (["ticket_class", "siblings", "spouse", "parents", "children"].includes(name)) {
            this.setState({
                [name]: Number(value),
                fare: curr_fare
            });
        } else {
            this.setState({
                [name]:value
            });
        }

    }

    handleSubmit(event) {
        event.preventDefault();
        fetch("https://surviveontitanic.herokuapp.com/survival/?ticket_class=" + this.state.ticket_class +
            "&sex=" + this.state.sex +
            "&age=" + this.state.age +
            "&siblings=" + this.state.siblings +
            "&spouse=" + this.state.spouse +
            "&parents=" + this.state.parents +
            "&children=" + this.state.children +
            "&fare=" + this.state.fare +
            "&embarked=" + this.state.embarked)
            .then(response => response.json())
            .then(response => {
                const survival = response["predict"] === 1;
                this.setState({
                    survive: survival
                })})
                    .then(
                        this.setState({
                            hasSubmitted: true
                        })
                    )
                    .catch(err => {
                        console.log("Could not fetch results", err);
                    });
    }

    handleClick() {
        this.setState({
            hasSubmitted: false,
            survive: false
        })
    }

    render() {
        return (
            <div className="content-div">
                <h2>Would you survive on the Titanic?</h2>
                <div className="content" id="input">
                    {this.state.hasSubmitted === true ?
                        <div id="results">
                            <h3>You {this.state.survive === false ? "did not survive!" : this.state.survive === true ? "survived!" : "???"}</h3>
                            <button className="submit-button" onClick={this.handleClick}>Submit another ticket form</button><br />
                            <ShareResults survive={this.state.survive}/>
                        </div> :
                        <div>
                            <h3>Ticket Form</h3>
                            <form onSubmit={this.handleSubmit}>
                                Class:
                                <input
                                type="radio"
                                name="ticket_class"
                                value="1"
                                checked={this.state.ticket_class === 1}
                                onChange={this.handleChange}
                                />1st
                                <input
                                type="radio"
                                name="ticket_class"
                                value="2"
                                checked={this.state.ticket_class === 2}
                                onChange={this.handleChange}
                                />2nd
                                <input
                                type="radio"
                                name="ticket_class"
                                value="3"
                                checked={this.state.ticket_class === 3}
                                onChange={this.handleChange}
                                />3rd<br />

                                Sex:
                                <select
                                name="sex"
                                value={this.state.sex}
                                onChange={this.handleChange}
                                >
                                <option value="female">Female</option>
                                <option value="male">Male</option>
                                <option value="other">Other</option>
                                </select> <br />
                                Siblings:
                                <input
                                className="number-input"
                                type="number"
                                min="0"
                                name="siblings"
                                value={this.state.siblings}
                                onChange={this.handleChange}
                                />
                                Spouse:
                                <input
                                className="number-input"
                                type="number"
                                min="0"
                                name="spouse"
                                value={this.state.spouse}
                                onChange={this.handleChange}
                                /> <br />
                                Parents:
                                <input
                                className="number-input"
                                type="number"
                                min="0"
                                name="parents"
                                value={this.state.parents}
                                onChange={this.handleChange}
                                />
                                Children:
                                <input
                                className="number-input"
                                type="number"
                                min="0"
                                name="children"
                                value={this.state.children}
                                onChange={this.handleChange}
                                /> <br />
                                Departure Port:
                                <select
                                name="port"
                                value={this.state.port}
                                onChange={this.handleChange}
                                >
                                <option value="C">Cherbourg</option>
                                <option value="Q">Queenstown</option>
                                <option value="S">Southampton</option>
                                </select><br />
                                <button className="submit-button">Submit Ticket</button>
                            </form>
                        </div>
                    }

                </div>
            </div>
        );
    }
}

export default InputData;