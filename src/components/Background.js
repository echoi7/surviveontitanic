import React, {Component} from 'react';
import splashAuth from '../authentication';
import BackgroundImage from './BackgroundImage';
import ImageCredit from "./ImageCredit";


class Background extends Component {
    constructor() {
        super();
        this.state = {
            imgFailed: false,
            imgURL:"",
            user:"",
            location:"",
            description:"",
            links:""
        };
        this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount() { //Where we grab random ship images from unsplash
        fetch("https://api.unsplash.com/photos/random/?query=ship&count=1&client_id=" + splashAuth)
            .then( response => response.json())
            .then(response => {
                const {urls, description, user, location, links, } = response[0];
                this.setState({
                    imgURL: urls,
                    user: user,
                    location: location,
                    description: description,
                    links: links
                });
            })
            .catch(err => {
                console.log("Could not fetch photo", err);
                this.setState({
                    imgFailed: true
                });
            })
    }

    render() {
        return (
            <div>
                {
                    this.state.imgFailed === false ?
                        <div>
                            <ImageCredit
                                link={this.state.links.html}
                                photographer={this.state.user.name}
                                city={this.state.location.city}
                                country={this.state.location.country}
                            />
                            <BackgroundImage url={this.state.imgURL.regular} />
                        </div>:
                        <div
                            className="bg"
                            id="bg-color"
                        />
                }
            </div>
        );
    }
}

export default Background;