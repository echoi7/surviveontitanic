from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache

from ..apps import WebappConfig
from django.http import HttpResponse, JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from ..apps import WebappConfig

import pandas as pd

class call_model(APIView):

    def get(self, request):
        if request.method == 'GET':
            sexMap = {
                "female":0,
                "male":1,
                "other":2
            }
            embarkedMap = {
                "c":0,
                "q":1,
                "s":2
            }
            ticket_class = request.GET.get('ticket_class')
            sex = request.GET.get('sex')
            age = request.GET.get('age')
            siblings = request.GET.get('siblings')
            spouse = request.GET.get('spouse')
            parents = request.GET.get('parents')
            children = request.GET.get('children')
            fare = request.GET.get('fare')
            embarked = request.GET.get('embarked')
            paremsMatrix = [
                [float(ticket_class), sexMap[sex], float(age), float(siblings)+float(spouse), float(parents)+float(children), float(fare), embarkedMap[embarked.lower()]]
            ]
            params = pd.DataFrame(paremsMatrix)
            predict = WebappConfig.model.predict(params)
            response = {
                "status_code": 500
            }
            if predict:
                response = {
                    "status_code": 200,
                    "predict": int(predict[0])
                }
            else:
                print(predict)


            return JsonResponse(response)
