from django.apps import AppConfig
import pickle
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class WebappConfig(AppConfig):

    modelFile = open(os.path.join(BASE_DIR, 'backend', 'titanic.p'), "rb")
    model = pickle.load(modelFile)
