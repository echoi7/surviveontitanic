# Would you survive on the Titanic?

This is a web application that quenches your curiosity: would you have survived on the Titanic?

## How it works

The app uses a machine learning model trained on the Titanic survivorship dataset on the Django back-end. It takes in user's information such as family members, age, and sex among other things to figure out whether the user would have survived the demise of the seemingly undestructible ship. 

## Are you ready to find out your fate on the Titanic?
Head on over to http://surviveontitanic.heroku.com to find out whether you would've survived! 
If you like this project, be sure to favorite this repo and share your results with your friends to see who would've survived.